#include <Array.au3>
#include <File.au3>

#RequireAdmin

Global $g_Config_GERAL_updatePath = IniRead("config.ini","GERAL","update_path", "")
Global $g_Config_GERAL_user       = IniRead("config.ini","GERAL","user", "")
Global $g_Config_GERAL_passwd     = IniRead("config.ini","GERAL","passwd", "")

#Region UpdateSetup
  Local $sUpdateSetup_windowTitle = "Assistente de Atualização - Domínio Contábil"

  Func UpdateSetup_run()
    Local $aPaths       = _FileListToArray($g_Config_GERAL_updatePath, Default, Default, True)
    Local $sUpdateFiles = _FileListToArray($aPaths[UBound($aPaths) - 1], Default, Default, True)
    Local $sUpdateFile  = $sUpdateFiles[UBound($sUpdateFiles) - 1]
    Local $aSplit       = StringSplit($sUpdateFile, "\")

    Run($sUpdateFile)
    WinWait($sUpdateSetup_windowTitle)
    Local $sSetup = WinGetHandle($sUpdateSetup_windowTitle)

    Local $iSteps = $aSplit[UBound($aSplit) - 1] = "Atualiza.exe" ? 4 : 3
    For $i = 0 To $iSteps Step +1
      Send("{enter}")
    Next

    While True
      Local $sCText = ControlGetText($sSetup, "", "[CLASS:TNewStaticText; INSTANCE:3]")
      If StringInStr($sCText, "Atualização Terminada") > 0 Then
        ControlClick($sSetup, "", "[CLASS:TNewButton; INSTANCE:3]")
        ExitLoop
      EndIf
    WEnd

  EndFunc
#EndRegion

#Region DBManager
  Local $sDBManager_executable = "C:\Program Files\Domínio Sistemas\Servidor Dominio\servidordominio.exe"
  Local $sDBManager_windowTitle = "Gerenciar Servidor de Banco de Dados"

  Func DBManager_open()
    Run($sDBManager_executable)
    WinMove(WinWait($sDBManager_windowTitle), "", 0, 0)
  EndFunc

  Func DBManager_close()
    WinClose(WinGetHandle($sDBManager_windowTitle))
  EndFunc

  Func DBManager_startService()
    MouseClick("left", 82, 430)
    Main_confirmNotice()
  EndFunc

  Func DBManager_stopService()
    MouseClick("left", 170, 430)
    Main_confirmNotice()
  EndFunc

  Func DBManager_restartService()
    DBManager_open()
    DBManager_stopService()
    DBManager_startService()
    DBManager_close()
  EndFunc

  Func DBManager_cormNotice()
    ControlClick(WinWait("Aviso"), "", "[CLASS:Button; INSTANCE:1]")
  EndFunc
#EndRegion

#Region Contabil
  Local $sContabil_executable = "C:\Contabil\contabil.exe /contabilidade"
  Local $sContabil_process = "contabil.exe"

  Func Contabil_open()
    Run($sContabil_executable)
    Local $sLoginWindow = WinWait("Conectando ...")
    
    ControlSetText($sLoginWindow, "", "[CLASS:Edit; INSTANCE:1]", $g_Config_GERAL_user)
    ControlSetText($sLoginWindow, "", "[CLASS:Edit; INSTANCE:2]", $g_Config_GERAL_passwd)
    ControlClick($sLoginWindow, "", "[CLASS:Button; INSTANCE:4]")

    Local $sUpdateAlertWindow = WinWait("Aviso", "", 30)
    
    If $sUpdateAlertWindow <> 0 Then
      Local $sUpdateAlertWindowText = ControlGetText($sUpdateAlertWindow, "", "[CLASS:Static; INSTANCE:2]")
      If StringInStr($sUpdateAlertWindowText, "Sua versão do banco de dados") > 0 Then
        ControlClick($sUpdateAlertWindow, "", "[CLASS:Button; INSTANCE:1]")
        ControlClick(WinWait("Cópia de segurança"), "", "[CLASS:Button; INSTANCE:5]")
        Main_confirmNotice()
      EndIf
    EndIf
  EndFunc

  Func Contabil_close()
    Main_killProcess($sContabil_process)
  EndFunc
#EndRegion

#Region CommunicationAgent
  Local $sCommunicationAgent_executable = "C:\Contabil\Agente de Comunicação com o Domínio Atendimento\Agente_comunicacao.exe"
  Local $sCommunicationAgent_process = "Agente_comunicacao.exe"

  Func CommunicationAgent_run()
    Run($sCommunicationAgent_executable)
    WinClose(WinWait("[CLASS:WindowsForms10.Window.8.app.0.378734a]"))
    Local $alert = WinWait("Atenção", "", 30)
    If $alert <> 0 Then
      ControlClick($alert, "", "[CLASS:Button; INSTANCE:2]")
    EndIf
  EndFunc

  Func CommunicationAgent_close()
    Main_killProcess($sCommunicationAgent_process)
  EndFunc
#EndRegion

#Region Main

  ; Terminate all system processes
  Contabil_close()

  ; Terminate the Communication Agent
  CommunicationAgent_close()

  ; Run the update setup
  UpdateSetup_run()

  ; Restart the database service
  DBManager_restartService()

  ; Start the system and perform the database upgrade if there is one
  Contabil_open()

  ; Close the system
  Contabil_close()

  ; Run the Communication Agent
  CommunicationAgent_run()

  Func Main_confirmNotice()
    ControlClick(WinWait("Aviso"), "", "[CLASS:Button; INSTANCE:1]")
  EndFunc

  Func Main_killProcess($_sProcess)
    While ProcessExists($_sProcess)
	    ProcessClose($_sProcess)
    WEnd
  EndFunc
#EndRegion